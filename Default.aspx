﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GoodReadsTwo._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div>
            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
                <telerik:RadSearchBox ID="RadSearchBox1" runat="server" Width="300" EmptyMessage="Title" OnButtonCommand="RadSearchBox1_ButtonCommand" OnSearch="RadSearchBox1_Search">
                    <DropDownSettings Height="400" Width="300"></DropDownSettings>
                </telerik:RadSearchBox>
            </telerik:RadAjaxPanel>
            <telerik:RadLabel ID="RadLabel1" runat="server"></telerik:RadLabel>
        </div>
    <asp:Repeater ID="Repeater1" runat="server">
        <HeaderTemplate>
            Genres
            <ul style="list-style-type:none">
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl='<%# string.Format("BookWithGenre.aspx?selectedGenre={0}", Eval("GenreId")) %>' Text='<%# Eval("Name") %>'>LinkButton</asp:LinkButton>
            </li>
        </ItemTemplate>
        <FooterTemplate>
        </ul>
        </FooterTemplate>
    </asp:Repeater>
    </div>
</asp:Content>
