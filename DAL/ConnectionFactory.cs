﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.DAL
{
    public class ConnectionFactory : IConnectionFactory
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["MyDBCS"].ConnectionString;
        public IDbConnection GetConnection
        {
            get
            {
                DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.SqlClient");
                DbConnection dbConnection = factory.CreateConnection();
                dbConnection.ConnectionString = _connectionString;
                dbConnection.Open();
                return dbConnection;
            }
        }
    }
}