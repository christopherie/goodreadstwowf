﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration.Conventions;
using GoodReadsTwo.Model;
using System.Data;
using System.Configuration;
using System.Data.Common;

namespace GoodReadsTwo.DAL
{
    public class GoodReadsContext : DbContext
    {
        public GoodReadsContext() : base("name=MyDBCS")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<Genre> Genres { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<BookImage> BookImages { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<Reviewer> Reviewers { get; set; }
        public DbSet<Status> Statuses { get; set; }
    }
}