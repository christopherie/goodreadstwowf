﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="GoodReadsTwo.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="Scripts/DataDashboard/genre-book-count.js" type="text/javascript"></script>

    <div class="row">
        <div class="panel-heading">
            <div class="col-md-8">
                <h3>
                    <i class="fa fa-pie-chart"></i>
                    <span>Data Dashboard </span>
                    <span>
                        Number of Books in Database: <telerik:RadLabel ID="RadLabel1" runat="server"></telerik:RadLabel>
                    </span>
                </h3>
            </div>
        </div>
    </div>

    <div class="row">
        <section class="col-md-6">
                
            <div class="chart">
                <div id="genre-book-count"></div>
            </div>

        </section>
    </div>
</asp:Content>
