﻿google.load('visualization', '1.0', { 'packages': ['corechart'] });

$(document).ready(function () {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        url: 'Dashboard.aspx/GetGenreData',
        success:
            function (response) {
                var options = {
                    title: 'Genre-Book Count',
                    width: '100%'
                };
                drawGraph(response.d, options, 'genre-book-count');
            }
    });
});

function drawGraph(dataValues, options, elementId) {
    // Initialization.  
    var data = new google.visualization.DataTable();

    // Setting.  
    data.addColumn('string', 'Genre');
    data.addColumn('number', 'Books with genre');


    // Processing.  
    for (var i = 0; i < dataValues.length; i++) {
        // Setting.  
        data.addRow([dataValues[i].Name, dataValues[i].BooksWithGenre]);
    }

    // Setting label.  
    var view = new google.visualization.DataView(data);
    view.setColumns([0, 1,
        {
            calc: "stringify",
            sourceColumn: 1,
            type: "string",
            role: "annotation"
        }
    ]);

    // Instantiate and draw our chart, passing in some options.  
    var chart = new google.visualization.PieChart(document.getElementById(elementId));

    // Draw chart.  
    chart.draw(view, options);
}  