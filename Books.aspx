﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Books.aspx.cs" Inherits="GoodReadsTwo.Books" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
 
        function validateImageUpload(sender, args) {
            var upload = $find("<%= RadAsyncUpload1.ClientID %>");
            args.IsValid = uploadImage.getUploadedFiles().length != 0;
        }

        function onClientFileSelected(sender, args) {
            var currentFileName = args.get_fileName();
            alert(currentFileName);
        }

    </script>
    

    <div id="dataModel"></div>

    <telerik:RadTextBox ID="RadTextBox1" runat="server"></telerik:RadTextBox><br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="RadTextBox1" Display="Dynamic"></asp:RequiredFieldValidator><br />
    <telerik:RadTextBox ID="RadTextBox2" runat="server"></telerik:RadTextBox><br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="RadTextBox2" Display="Dynamic"></asp:RequiredFieldValidator><br />
    <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList><br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="DropDownList1" Display="Dynamic"></asp:RequiredFieldValidator><br />
    <telerik:RadAsyncUpload ID="RadAsyncUpload1" runat="server" Skin="Bootstrap">
    </telerik:RadAsyncUpload><br />
    <telerik:RadButton ID="RadButton1" runat="server" Text="Add Book" OnClick="RadButton1_Click"></telerik:RadButton>
</asp:Content>
