﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Genres.aspx.cs" Inherits="GoodReadsTwo.Genres" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <telerik:RadLabel ID="RadLabel1" runat="server"></telerik:RadLabel>
        <br />
        <telerik:RadTextBox ID="RadTextBox1" runat="server"></telerik:RadTextBox><br />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="A Genre is required" ControlToValidate="RadTextBox1"></asp:RequiredFieldValidator><br />
        <telerik:RadButton ID="RadButton1" runat="server" Text="Add Genre" OnClick="RadButton1_Click"></telerik:RadButton>
    </div>
</asp:Content>
