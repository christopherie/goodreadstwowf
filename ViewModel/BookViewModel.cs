﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.ViewModel
{
    public class BookViewModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public byte[] Image { get; set; }
    }
}