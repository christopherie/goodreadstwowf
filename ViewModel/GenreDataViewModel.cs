﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.ViewModel
{
    public class GenreDataViewModel
    {
        public string Name { get; set; }
        public int BooksWithGenre { get; set; }
    }
}