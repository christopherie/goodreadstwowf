﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Model
{
    public class Permission
    {
        public int PermissionID { get; set; }

        [DisplayName("Permission")]
        public string Name { get; set; }


        public virtual ICollection<Role> Roles { get; set; }
    }
}