﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace GoodReadsTwo.Model
{
    public class Book
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Required")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Required")]
        public string Author { get; set; }

        public int GenreId { get; set; }

        public virtual Genre Genre { get; set; }
        public virtual BookImage BookImage { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }

        public virtual ICollection<Review> Reviews { get; set; }
    }
}