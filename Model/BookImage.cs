﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Model
{
    public class BookImage
    {
        [Key]
        [ForeignKey("Book")]
        public int ID { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public string ContentType { get; set; }


        public virtual Book Book { get; set; }
    }
}