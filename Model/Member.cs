﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Model
{
    public class Member
    {
        [DisplayName("Email")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string MemberID { get; set; }

        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }


        public virtual ICollection<Review> Reviews { get; set; }
    }
}