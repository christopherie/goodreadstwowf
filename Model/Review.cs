﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Model
{
    public class Review
    {
        public int ReviewID { get; set; }
        public double Rating { get; set; }
        public string Content { get; set; }
        public DateTime ReviewDate { get; set; }

        [ForeignKey("Reviewer")]
        public string ReviewerID { get; set; }

        public string Status { get; set; }

        public virtual Book Book { get; set; }
        public virtual Reviewer Reviewer { get; set; }
        public virtual Member Member { get; set; }
    }
}