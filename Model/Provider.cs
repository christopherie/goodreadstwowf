﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Model
{
    public class Provider
    {
        public int ProviderID { get; set; }

        [DisplayName("Provider Type")]
        public string Name { get; set; }


    }
}