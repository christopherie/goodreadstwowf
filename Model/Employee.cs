﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Model
{
    public class Employee
    {
        [DisplayName("Email")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string EmployeeID { get; set; }
        public int RoleID { get; set; }

        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayName("Date of Birth")]
        public DateTime DateOfBirth { get; set; }


        public virtual Role Role { get; set; }
    }
}