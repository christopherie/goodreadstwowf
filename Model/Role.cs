﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Model
{
    public class Role
    {
        public int RoleID { get; set; }

        [DisplayName("Role Name")]
        public string Name { get; set; }


        public virtual ICollection<Employee> Employees { get; set; }
        public virtual ICollection<Permission> Permissions { get; set; }
    }
}