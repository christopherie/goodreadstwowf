﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Model
{
    public class Status
    {
        public int StatusID { get; set; }

        public string Name { get; set; }
    }
}