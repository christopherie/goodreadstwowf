﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Model
{
    public class Tag
    {
        public int TagID { get; set; }
        public string Name { get; set; }


        public virtual ICollection<Book> Books { get; set; }
    }
}