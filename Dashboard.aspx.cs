﻿using Dapper;
using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using GoodReadsTwo.Repository;
using GoodReadsTwo.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodReadsTwo
{
    public partial class Dashboard : System.Web.UI.Page
    {
        private static readonly GoodReadsContext _goodReadsContext;
        private static readonly UnitOfWork _unitOfWork;
        private static readonly SqlConnection _sqlConnection;

        public GoodReadsContext GoodReadsContext => _goodReadsContext;

        public UnitOfWork UnitOfWork => _unitOfWork;

        public SqlConnection SqlConnection => _sqlConnection;

        static Dashboard()
        {
            _goodReadsContext = new GoodReadsContext();
            _unitOfWork = new UnitOfWork(_goodReadsContext);
            _sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDBCS"].ConnectionString);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (IDbConnection db = _sqlConnection)
            {
                int counter = db.ExecuteScalar<int>("SELECT COUNT(ID) AS Total FROM[GoodReadsTwo].[dbo].[Book]");
                RadLabel1.Text = counter.ToString();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static List<GenreDataViewModel> GetGenreData()
        {
            List<GenreDataViewModel> genreData = new List<GenreDataViewModel>();

            List<Genre> graphData = _unitOfWork.Repository<Genre>().GetAll().ToList();

            genreData = graphData.Select(p => new GenreDataViewModel { Name = p.Name, BooksWithGenre = p.Books.Count }).ToList();

            return genreData;
        }
    }
}