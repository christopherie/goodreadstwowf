﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using GoodReadsTwo.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodReadsTwo
{
    public partial class NewEmployee : System.Web.UI.Page
    {
        IEmployeeRepository _employeeRepository;
        IRoleRepository _roleRepository;
        IConnectionFactory _connectionFactory;
        GoodReadsContext _goodReadsContext;

        public NewEmployee()
        {
            _connectionFactory = new ConnectionFactory();
            _employeeRepository = new EmployeeRepository(_connectionFactory);
            _roleRepository = new RoleRepository(_goodReadsContext);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            IEnumerable<Role> roles = _roleRepository.GetAll();
            RadDropDownList1.DataSource = roles;
            RadDropDownList1.DataTextField = "Name";
            RadDropDownList1.DataValueField = "RoleID";
            RadDropDownList1.DataBind();

            RadDatePicker1.FocusedDate = DateTime.Now;
            RadDatePicker1.MinDate = DateTime.Now.AddYears(-80);
            RadDatePicker1.MaxDate = DateTime.Now;

            RadButton1.Click += RadButton1_Click;
        }

        private void RadButton1_Click(object sender, EventArgs e)
        {
            if (ModelState.IsValid)
            {

                Employee employee = new Employee();
                employee.EmployeeID = RadTextBox1.Text.Trim();
                employee.FirstName = RadTextBox2.Text.Trim();
                employee.LastName = RadTextBox3.Text.Trim();
                string dob = RadDatePicker1.SelectedDate.Value.ToString("yyyy/MM/dd");
                employee.DateOfBirth = Convert.ToDateTime(dob);
                employee.RoleID = Convert.ToInt32(RadDropDownList1.SelectedValue);
                _employeeRepository.Insert(employee);
            }
        }
    }
}