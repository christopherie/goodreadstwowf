﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AllBooks.aspx.cs" Inherits="GoodReadsTwo.AllBooks" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script src="Scripts/jquery.signalR-2.4.1.min.js"></script>
    <script type="text/javascript">  
        $(function () {
            var hubNotify = $.connection.bookHub;

            $.connection.hub.start().done(function () {
                getAll();
            });

            hubNotify.client.refreshBookData = function () {
                getAll();
            };
        });

        function getAll() {
            var model = $('#dataModel');
            $.ajax({
                url: '/AllBooks/GetBookData',
                contentType: 'application/html ; charset:utf-8',
                type: 'GET',
                dataType: 'html',
                success: function (result) { model.empty().append(result); }
            });
        }  
</script>  

    <div id="datamodel"></div>

    <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource1">
        <ItemTemplate>
            <asp:Label ID="Label1" runat="server" Text='<%#Eval("Title") %>'></asp:Label><br />
        </ItemTemplate>
    </asp:Repeater>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="Data Source=DESKTOP-B7PVH9B\SQLEXPRESS;Initial Catalog=GoodReadsTwo;Integrated Security=True" 
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT [ID], [Title], [Author] FROM [Book]">
    </asp:SqlDataSource>
</asp:Content>
