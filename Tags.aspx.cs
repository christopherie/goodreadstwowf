﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using GoodReadsTwo.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace GoodReadsTwo
{
    public partial class Tags : System.Web.UI.Page
    {
        private readonly GoodReadsContext _goodReadsContext;
        private readonly UnitOfWork _unitOfWork;

        public Tags()
        {
            _goodReadsContext = new GoodReadsContext();
            _unitOfWork = new UnitOfWork(_goodReadsContext);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            List<Tag> tags = _unitOfWork.Repository<Tag>().GetAll().ToList();


            foreach (var item in tags)
            {
                RadCheckBoxList1.Items.Add(new ButtonListItem(item.Name, item.TagID.ToString())); 
            }
        }
    }
}