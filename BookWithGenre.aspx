﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BookWithGenre.aspx.cs" Inherits="GoodReadsTwo.BookWithGenre" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:Label ID="Label1" runat="server"></asp:Label>

    <asp:Repeater ID="Repeater1" runat="server">
        <HeaderTemplate>
            <ul style="list-style-type:none">
        </HeaderTemplate>
        <ItemTemplate>
            <asp:ImageButton ID="ImageButton1" runat="server" PostBackUrl='<%# string.Format("BookInGenre.aspx?selectedBookInGenre={0}", Eval("ID")) %>' ImageUrl='<%# GetImage(Eval("BookImage.Image")) %>'/>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>

</asp:Content>
