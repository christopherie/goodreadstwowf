﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using GoodReadsTwo.Repository;
using GoodReadsTwo.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodReadsTwo
{
    public partial class BookWithGenre : System.Web.UI.Page
    {
        private GoodReadsContext _goodReadsContext;
        private UnitOfWork _unitOfwork;

        public BookWithGenre()
        {
            _goodReadsContext = new GoodReadsContext();
            _unitOfwork = new UnitOfWork(_goodReadsContext);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int queryString = Convert.ToInt32(Page.Request.QueryString.Get("selectedGenre"));

            Genre genre = _unitOfwork.Repository<Genre>().Find(b => b.GenreId == queryString).Single();

            // Just for fun a viewmodel and sproc :)
            // List<BookViewModel> books = _goodReadsContext.Database.SqlQuery<BookViewModel>("BooksInGenre @param1", new SqlParameter("param1", queryString)).ToList();

            // Could use dbcontext
            //List<Book> books = _goodReadsContext.Books.Include(c => c.BookImage).Where(c => c.GenreId == queryString).ToList();
            //List<Book> books = _goodReadsContext.Books.Include("BookImage").Where(c => c.GenreId == queryString).ToList();

            // Best option to add this method in IRepository
            _goodReadsContext.Database.Log = l => Debug.Write(l);
            List<Book> books = _unitOfwork.Repository<Book>().Find(b => b.GenreId == queryString, "BookImage").ToList();
            books.AsReadOnly();

            Label1.Text = genre.Name;
            Repeater1.DataSource = books;
            Repeater1.DataBind();
        }

        public string GetImage(object img)
        {
            return "data:image/jpg;base64," + Convert.ToBase64String((byte[])img);
        }
    }
}