﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using GoodReadsTwo.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace GoodReadsTwo
{
    public partial class _Default : Page
    {
        private readonly GoodReadsContext _goodReadsContext;
        private readonly UnitOfWork _unitOfWork;

        public _Default()
        {
            _goodReadsContext = new GoodReadsContext();
            _unitOfWork = new UnitOfWork(_goodReadsContext);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            IEnumerable<Genre> genres = _unitOfWork.Repository<Genre>().GetAll();
            Repeater1.DataSource = genres;
            Repeater1.DataBind();

            IEnumerable<Book> books = _unitOfWork.Repository<Book>().GetAll();
            RadSearchBox1.DataSource = books;
            RadSearchBox1.DataTextField = "Title";
            RadSearchBox1.DataValueField = "ID";
            RadSearchBox1.DataBind();
        }

        protected void RadSearchBox1_ButtonCommand(object sender, SearchBoxButtonEventArgs e)
        {
            RadLabel1.Text = e.CommandArgument.Trim();
        }

        protected void RadSearchBox1_Search(object sender, SearchBoxEventArgs e)
        {
            string title = e.Text.Trim();
            string id = e.Value;

            Response.Redirect("Search.aspx?title=" + title + "&id=" + id, false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }
}