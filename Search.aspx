﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="GoodReadsTwo.Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadLabel ID="RadLabel1" runat="server"></telerik:RadLabel><br />
    <telerik:RadLabel ID="RadLabel2" runat="server"></telerik:RadLabel><br />
    <telerik:RadLabel ID="RadLabel3" runat="server"></telerik:RadLabel><br />
    <telerik:RadBinaryImage ID="RadBinaryImage1" runat="server" /><br />

    <asp:Repeater ID="Repeater1" runat="server">
        <HeaderTemplate>
            <ul style="list-style-type:none">
        </HeaderTemplate>
        <ItemTemplate>
            <li><telerik:RadRating ID="RadRating1" runat="server" Value='<%# GetRating(Eval("Rating")) %>' Precision="Exact"></telerik:RadRating></li>
            <li><telerik:RadLabel ID="RadLabel4" runat="server" Text='<%# FormatDate(Eval("ReviewDate")) %>'></telerik:RadLabel></li>
                <li><telerik:RadLabel ID="RadLabel5" runat="server" Text='<%# Eval("Content") %>'></telerik:RadLabel></li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater><br />

    <telerik:RadButton ID="RadButton1" runat="server" Text="Edit Book" OnClick="RadButton1_Click"></telerik:RadButton>
</asp:Content>
