﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Repository
{
    public class BookRepository : Repository<Book>, IBookRepository
    {
        public GoodReadsContext GoodReadsContext => dbContext as GoodReadsContext;

        public BookRepository(GoodReadsContext context) : base(context)
        {

        }
    }
}