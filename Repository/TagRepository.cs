﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Repository
{
    public class TagRepository : Repository<Tag>, ITagRepository
    {
        public TagRepository(GoodReadsContext context) : base(context)
        {
        }

        public GoodReadsContext Context => dbContext as GoodReadsContext;
    }
}