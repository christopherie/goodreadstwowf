﻿using Dapper;
using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Repository
{
    public class EmployeeRepository : Repository<Employee>, IEmployeeRepository
    {
        public IConnectionFactory ConnectionFactory { get; }

        public EmployeeRepository(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
            ConnectionFactory = connectionFactory;
        }


        public void Insert(Employee employee)
        {
            var sql = "uspInsertEmployee";

            using (IDbConnection connection = ConnectionFactory.GetConnection)
            using (IDbTransaction transaction = connection.BeginTransaction())
            {

                try
                {
                    DynamicParameters dynamicParameters = new DynamicParameters();
                    dynamicParameters.Add("@email", employee.EmployeeID);
                    dynamicParameters.Add("@roleid", employee.RoleID);
                    dynamicParameters.Add("@fn", employee.FirstName);
                    dynamicParameters.Add("@ln", employee.LastName);
                    dynamicParameters.Add("@dob", employee.DateOfBirth);
                    connection.Execute(sql, dynamicParameters, transaction, commandType: CommandType.StoredProcedure);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                }
            }
        }
    }
}