﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data;
using Dapper;
using System.Diagnostics;

namespace GoodReadsTwo.Repository
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        private readonly GoodReadsContext _context;
        private readonly UnitOfWork _unitOfWork;
        public IConnectionFactory ConnectionFactory { get; }

        public RoleRepository(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
            ConnectionFactory = connectionFactory;
        }

        public RoleRepository(GoodReadsContext context) : base(context)
        {
            _context = new GoodReadsContext();
            _unitOfWork = new UnitOfWork(_context);
        }

        public void Insert(string role)
        {
            var sql = "uspInsertRole";

            using (IDbConnection connection = ConnectionFactory.GetConnection)
            using (IDbTransaction transaction = connection.BeginTransaction())
            {

                try
                {
                    DynamicParameters dynamicParameters = new DynamicParameters();
                    dynamicParameters.Add("@Name", role);
                    connection.Execute(sql, dynamicParameters, transaction, commandType: CommandType.StoredProcedure);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    
                }
            }
        }
    }
}