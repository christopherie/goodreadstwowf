﻿using Dapper;
using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Repository
{
    public class PermissionRepository : Repository<Permission>, IPermissionRepository
    {
        public PermissionRepository(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
            ConnectionFactory = connectionFactory;
        }

        public IConnectionFactory ConnectionFactory { get; }

        public DataTable GetPermissionRoles()
        {
            var sql = "uspPermissionRole";

            using (SqlConnection sqlConnection = new SqlConnection(ConnectionFactory.GetConnection.ConnectionString))
            using (SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                try
                {
                    sqlConnection.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlDataReader);
                    return dataTable;
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
    }
}