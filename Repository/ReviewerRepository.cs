﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Repository
{
    public class ReviewerRepository : Repository<Reviewer>, IReviewerRepository
    {
        public GoodReadsContext GoodReadsContext => dbContext as GoodReadsContext;
        public IConnectionFactory ConnectionFactory { get; }

        public ReviewerRepository(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
            ConnectionFactory = connectionFactory;
        }

        public ReviewerRepository(GoodReadsContext context) : base(context)
        {
        }

        public DataTable GetActiveReviewers()
        {
            var sql = "uspGetAllActiveReviewers";

            using (SqlConnection sqlConnection = new SqlConnection(ConnectionFactory.GetConnection.ConnectionString))
            using (SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                try
                {
                    sqlConnection.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlDataReader);
                    return dataTable;
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
    }
}