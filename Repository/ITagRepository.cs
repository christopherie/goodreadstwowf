﻿using GoodReadsTwo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodReadsTwo.Repository
{
    public interface ITagRepository : IRepository<Tag>
    {
    }
}
