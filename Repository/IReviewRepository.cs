﻿using GoodReadsTwo.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodReadsTwo.Repository
{
    public interface IReviewRepository : IRepository<Review>
    {
        DataTable GetUnassignedReviews();
    }
}
