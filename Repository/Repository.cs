﻿using GoodReadsTwo.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace GoodReadsTwo.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected DbContext dbContext;

        protected IConnectionFactory connFactory;

        public Repository(IConnectionFactory connectionFactory)
        {
            connFactory = connectionFactory;
        }

        public Repository(DbContext context)
        {
            dbContext = context;
        }
        public void Add(T t)
        {
            dbContext.Set<T>().Add(t);
        }

        public void AddRange(IEnumerable<T> ts)
        {
            dbContext.Set<T>().AddRange(ts);
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> expression)
        {
            return dbContext.Set<T>().Where(expression);
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> expression, string children)
        {
            return dbContext.Set<T>().Include(children).Where(expression);
        }

        public T Get(int id)
        {
            return dbContext.Set<T>().Find(id);
        }

        public T Get(Expression<Func<T, bool>> expression)
        {
            return dbContext.Set<T>().Where(expression).Single();
        }

        public T Get(Expression<Func<T, bool>> expression, string children)
        {
            return dbContext.Set<T>().Include(children).Where(expression).Single();
        }

        public T Get(Func<T, bool> where, params Expression<Func<T, object>>[] expressions)
        {
            T item = null;

            IEnumerable<T> dbSet = dbContext.Set<T>();

            foreach (Expression<Func<T, object>> expression in expressions)
            {
                dbSet = dbSet.AsQueryable().Include(expression);
            }

            item = dbSet.FirstOrDefault(where);

            return item;
        }

        public IEnumerable<T> GetAll()
        {
            return dbContext.Set<T>().ToList();
        }

        public void Remove(T t)
        {
            dbContext.Set<T>().Remove(t);
        }

        public void RemoveRange(IEnumerable<T> ts)
        {
            dbContext.Set<T>().RemoveRange(ts);
        }
    }
}