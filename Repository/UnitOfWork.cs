﻿using GoodReadsTwo.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly GoodReadsContext _goodReadsContext;
        public Dictionary<Type, object> repositories = new Dictionary<Type, object>();
        private readonly IConnectionFactory _connectionFactory;
        public UnitOfWork(GoodReadsContext goodReadsContext)
        {
            _goodReadsContext = goodReadsContext;
        }
        public void Complete()
        {
            _goodReadsContext.SaveChanges();
        }

        public void Dispose()
        {
            _goodReadsContext.Dispose();
        }

        public IRepository<T> Repository<T>() where T : class
        {
            if (repositories.Keys.Contains(typeof(T)) == true)
            {
                return repositories[typeof(T)] as IRepository<T>;
            }

            IRepository<T> repo = new Repository<T>(_goodReadsContext);
            repositories.Add(typeof(T), repo);
            return repo;
        }
    }
}