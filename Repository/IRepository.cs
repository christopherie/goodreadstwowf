﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GoodReadsTwo.Repository
{
    public interface IRepository<T> where T : class
    {
        T Get(int id);
        T Get(Expression<Func<T, bool>> expression);
        T Get(Expression<Func<T, bool>> expression, string children);
        T Get(Func<T, bool> where, params Expression<Func<T, object>>[] expressions);
        IEnumerable<T> GetAll();
        IEnumerable<T> Find(Expression<Func<T, bool>> expression);
        IEnumerable<T> Find(Expression<Func<T, bool>> expression, string children);
        void Add(T t);
        void AddRange(IEnumerable<T> ts);
        void Remove(T t);
        void RemoveRange(IEnumerable<T> ts);
    }
}
