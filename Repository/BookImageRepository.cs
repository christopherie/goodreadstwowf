﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Repository
{
    public class BookImageRepository : Repository<BookImage>, IBookImageRepository
    {
        public  GoodReadsContext GoodReadsContext => dbContext as GoodReadsContext;

        public BookImageRepository(GoodReadsContext context) : base(context)
        {
        }
    }
}