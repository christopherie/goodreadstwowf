﻿using GoodReadsTwo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Repository
{
    public interface IBookRepository : IRepository<Book>
    {
    }
}