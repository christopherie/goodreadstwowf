﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Repository
{
    public class ReviewRepository : Repository<Review>, IReviewRepository
    {
        public GoodReadsContext GoodReadsContext => dbContext as GoodReadsContext;
        public IConnectionFactory ConnectionFactory { get; }

        public ReviewRepository(IConnectionFactory connectionFactory) : base(connectionFactory)
        {
            ConnectionFactory = connectionFactory;
        }

        public ReviewRepository(GoodReadsContext context) : base(context)
        {
        }

        public DataTable GetUnassignedReviews()
        {
            var sql = "uspGetAllUnassignedReviews";

            using (SqlConnection sqlConnection = new SqlConnection(ConnectionFactory.GetConnection.ConnectionString))
            using (SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                try
                {
                    sqlConnection.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    DataTable dataTable = new DataTable();
                    dataTable.Load(sqlDataReader);
                    return dataTable;
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        public void AssignReview(int reviewID)
        {
            var sql = "uspAssignReview";

            using (SqlConnection sqlConnection = new SqlConnection(ConnectionFactory.GetConnection.ConnectionString))
            using (SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                try
                {
                    sqlConnection.Open();
                    sqlCommand.ExecuteReader();

                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
    }
}