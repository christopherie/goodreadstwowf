﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Repository
{
    public class GenreRepository : Repository<Genre>, IGenreRepository
    {
        public GoodReadsContext GoodReadsContext => dbContext as GoodReadsContext;
        public GenreRepository(GoodReadsContext context) : base(context)
        {
        }
    }
}