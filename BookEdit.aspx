﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BookEdit.aspx.cs" Inherits="GoodReadsTwo.BookEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    Title<br />
    <telerik:RadTextBox ID="RadTextBox1" runat="server" AutoPostBack="false"></telerik:RadTextBox><br />
    Author<br />
    <telerik:RadTextBox ID="RadTextBox2" runat="server" AutoPostBack="false"></telerik:RadTextBox><br />
    Genre <br />
    <telerik:RadTextBox ID="RadTextBox3" runat="server" ReadOnly="true"></telerik:RadTextBox><br />
    Update Genre<br />
    <telerik:RadDropDownList ID="RadDropDownList1" runat="server" AutoPostBack="false"></telerik:RadDropDownList><br />
    Update Tags<br />
    <telerik:RadCheckBoxList ID="RadCheckBoxList1" runat="server" AutoPostBack="false"></telerik:RadCheckBoxList><br />
    <telerik:RadBinaryImage ID="RadBinaryImage1" runat="server" Width="25"/><br />
    Update Cover image<br />
    <telerik:RadAsyncUpload ID="RadAsyncUpload1" runat="server"></telerik:RadAsyncUpload><br />
    <telerik:RadButton ID="RadButton1" runat="server" Text="RadButton" OnClick="RadButton1_Click"></telerik:RadButton>
</asp:Content>
