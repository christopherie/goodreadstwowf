﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using GoodReadsTwo.Repository;
using GoodReadsTwo.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace GoodReadsTwo
{
    public partial class BookEdit : System.Web.UI.Page
    {
        private readonly GoodReadsContext _goodReadsContext;
        private readonly UnitOfWork _unitOfWork;

        public BookEdit()
        {
            _goodReadsContext = new GoodReadsContext();
            _unitOfWork = new UnitOfWork(_goodReadsContext);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int queryid = Convert.ToInt32(Page.Request.QueryString.Get("editId"));

                Book book = _unitOfWork.Repository<Book>().Get(b => b.ID == queryid, b => b.BookImage, b => b.Tags);

                RadTextBox1.Text = book.Title;

                RadTextBox2.Text = book.Author;

                Genre genre = _unitOfWork.Repository<Genre>().Get(g => g.GenreId == book.GenreId, "Books");

                RadTextBox3.Text = genre.Name;

                SetGenreDropDown();

                RadBinaryImage1.DataValue = book.BookImage.Image;

                SetAssignedTags(book); 
            }
        }

        private void SetGenreDropDown()
        {
            IEnumerable<Genre> genres = _unitOfWork.Repository<Genre>().GetAll();

            foreach (var item in genres)
            {
                RadDropDownList1.Items.Add(new DropDownListItem(item.Name, item.GenreId.ToString()));
            }
        }

        private void SetAssignedTags(Book book)
        {
            IEnumerable<Tag> allTags = _unitOfWork.Repository<Tag>().GetAll();

            HashSet<Tag> tags = new HashSet<Tag>(allTags);

            ICollection<Tag> bookTagsCollection = book.Tags;

            HashSet<int> bookTags = new HashSet<int>(bookTagsCollection.Select(c => c.TagID));

            foreach (var item in tags)
            {
                if (bookTags.Contains(item.TagID))
                {
                    RadCheckBoxList1.Items.Add(new ButtonListItem(item.Name, item.TagID.ToString(), true, true));
                }
                else
                {
                    RadCheckBoxList1.Items.Add(new ButtonListItem(item.Name, item.TagID.ToString(), true));
                }
            }
        }

        protected void RadButton1_Click(object sender, EventArgs e)
        {
            int? queryid = Convert.ToInt32(Page.Request.QueryString.Get("editId"));

            if (queryid == null)
            {
                Response.Status = "400 Bad Request";
            }

            Book book = _unitOfWork.Repository<Book>().Get(b => b.ID == queryid, b => b.BookImage, b => b.Tags);

            string[] selectedTags = RadCheckBoxList1.SelectedValues;

            UpdateAssignedTags(selectedTags, book);

            _goodReadsContext.Database.Log = l => Debug.Write(l);

            _goodReadsContext.Entry(book).State = EntityState.Modified;

            _goodReadsContext.SaveChanges();

            _goodReadsContext.Dispose();
        }

        private void UpdateAssignedTags(string[] selectedTags, Book book)
        {
            if (selectedTags == null)
            {
                book.Tags = new List<Tag>();
                return;
            }

            IEnumerable<Tag> allTags = _unitOfWork.Repository<Tag>().GetAll();

            HashSet<Tag> tags = new HashSet<Tag>(allTags);

            HashSet<string> selectedTagsHS = new HashSet<string>(selectedTags);

            HashSet<int> bookTagsHS = new HashSet<int>(book.Tags.Select(t => t.TagID));

            foreach (Tag item in tags)
            {
                if(selectedTagsHS.Contains(item.TagID.ToString()))
                {
                    if (!bookTagsHS.Contains(item.TagID))
                    {
                        book.Tags.Add(item);
                    }
                }
                else
                {
                    if (bookTagsHS.Contains(item.TagID))
                    {
                        book.Tags.Remove(item);
                    }
                }
            }
        }
    }
}