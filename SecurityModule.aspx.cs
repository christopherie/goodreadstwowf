﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodReadsTwo
{
    public partial class SecurityModule : System.Web.UI.Page
    {
        IConnectionFactory _connectionFactory;

        IPermissionRepository _permissionRepository;

        public SecurityModule()
        {
            _connectionFactory = new ConnectionFactory();
            _permissionRepository = new PermissionRepository(_connectionFactory);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }

            GridView1.RowEditing += GridView1_RowEditing;
            GridView1.RowCancelingEdit += GridView1_RowCancelingEdit;
            GridView1.RowUpdating += GridView1_RowUpdating;
            
        }

        private void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow gridViewRow = GridView1.Rows[e.RowIndex];
            int dataItemIndex = gridViewRow.DataItemIndex;
        }

        private void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            BindGrid();
        }

        private void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            BindGrid();
        }

        private void BindGrid()
        {
            GridView1.DataSource = _permissionRepository.GetPermissionRoles();
            GridView1.DataBind();
        }

        
    }
}