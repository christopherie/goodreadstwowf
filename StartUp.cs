﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Owin;

[assembly: OwinStartup(typeof(GoodReadsTwo.StartUp))]

namespace GoodReadsTwo
{
    public class StartUp
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}