﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using GoodReadsTwo.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace GoodReadsTwo
{
    public partial class AdminDashboard : System.Web.UI.Page
    {
        IConnectionFactory _connectionFactory;
        IReviewRepository _reviewRepository;
        
        public AdminDashboard()
        {
            _connectionFactory = new ConnectionFactory();
            _reviewRepository = new ReviewRepository(_connectionFactory);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RadGrid1.NeedDataSource += RadGrid1_NeedDataSource;
                RadGrid1.ItemDataBound += RadGrid1_ItemDataBound;
            }
        }

        private void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {

            
            
        }

        private void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RadGrid1.DataSource = _reviewRepository.GetUnassignedReviews();
        }

        protected void CheckBox1_CheckChanged(object sender, EventArgs e)
        {
            RadCheckBox checkBox = (RadCheckBox)sender;
            GridDataItem item = (GridDataItem)checkBox.NamingContainer;
            string reviewerID = item.GetDataKeyValue("ReviewerID").ToString();

            if (reviewerID != null)
            {

            }
        
            
            
        }

       

        protected void RadComboBox1_ItemsRequested(object sender, EventArgs e)
        {
            RadComboBox combo = (RadComboBox)sender;
            combo.ClearSelection();
            ReviewerRepository reviewerRepository = new ReviewerRepository(_connectionFactory);
            combo.DataSource = reviewerRepository.GetActiveReviewers();
            combo.DataTextField = "ReviewerName";
            combo.DataValueField = "ReviewerID";
            combo.DataBind();
            //combo.Items.Insert(0, new RadComboBoxItem("", "0"));
        }

        protected void RadComboBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}