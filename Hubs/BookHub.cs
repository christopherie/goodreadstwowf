﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodReadsTwo.Hubs
{
    [HubName("bookHub")]
    public class BookHub : Hub
    {
        public void BroadCastData()
        {
            IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<BookHub>();
            hubContext.Clients.All.refreshBookData();
        }
    }
}