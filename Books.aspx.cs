﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Hubs;
using GoodReadsTwo.Model;
using GoodReadsTwo.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Web.Script.Services;
using System.Web.Services;
using Telerik.Web.UI;

namespace GoodReadsTwo
{
    public partial class Books : System.Web.UI.Page
    {
        private readonly GoodReadsContext _goodReadsContext;
        private readonly UnitOfWork _unitOfwork;

        public Books()
        {
            _goodReadsContext = new GoodReadsContext();
            _unitOfwork = new UnitOfWork(_goodReadsContext);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            IEnumerable<Genre> genres = _unitOfwork.Repository<Genre>().GetAll();
            DropDownList1.DataSource = genres;
            DropDownList1.DataTextField = "Name";
            DropDownList1.DataValueField = "GenreId";
            DropDownList1.DataBind();
        }

        protected void RadButton1_Click(object sender, EventArgs e)
        {
            if (ModelState.IsValid)
            {
                Book book = new Book
                {
                    Title = RadTextBox1.Text.Trim(),
                    Author = RadTextBox2.Text.Trim(),
                    GenreId = Convert.ToInt32(DropDownList1.SelectedValue)
                };

                BookImage bookImage = null;

                if (RadAsyncUpload1.UploadedFiles.Count > 0)
                {
                    UploadedFileCollection uploadedFileCollection = RadAsyncUpload1.UploadedFiles;

                    for (int i = 0; i < uploadedFileCollection.Count; i++)
                    {
                        bookImage = new BookImage
                        {
                            Name = uploadedFileCollection[i].FileName,
                            ContentType = uploadedFileCollection[i].ContentType
                        };
                        Stream stream = uploadedFileCollection[i].InputStream;
                        BinaryReader binaryReader = new BinaryReader(stream);
                        byte[] bytes = binaryReader.ReadBytes((int)stream.Length);
                        bookImage.Image = bytes;
                    }
                }

                _goodReadsContext.Database.Log = l => Debug.WriteLine(l);
                _unitOfwork.Repository<Book>().Add(book);
                _unitOfwork.Repository<BookImage>().Add(bookImage);
                _unitOfwork.Complete();
                BookHub bookHub = new BookHub();
                bookHub.BroadCastData();

                Response.Redirect("Contact.aspx", false);
                Context.ApplicationInstance.CompleteRequest();
            }
        }
    }
}