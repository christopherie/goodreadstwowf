﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using GoodReadsTwo.Repository;
using System;
using System.Data.Entity.Validation;

namespace GoodReadsTwo
{
    public partial class Genres : System.Web.UI.Page
    {
        private readonly GoodReadsContext _goodReadsContext;
        private readonly UnitOfWork _unitOfWork;

        public Genres()
        {
            _goodReadsContext = new GoodReadsContext();
            _unitOfWork = new UnitOfWork(_goodReadsContext);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            RadTextBox1.AutoPostBack = false;
            RadLabel1.Text = "Create Genre";
        }

        protected void RadButton1_Click(object sender, EventArgs e)
        {
            if (ModelState.IsValid)
            {
                Genre genre = new Genre
                {
                    Name = RadTextBox1.Text.Trim()
                };
                _unitOfWork.Repository<Genre>().Add(genre);
                _unitOfWork.Complete();

                Response.Redirect("Contact.aspx");
            }
        }
    }
}