﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace GoodReadsTwo.CustomControls
{
    public partial class TestControl : System.Web.UI.UserControl
    {
        IConnectionFactory _connectionFactory;

        public TestControl()
        {
            _connectionFactory = new ConnectionFactory();
        }

        RadComboBoxItemCollection radComboBoxItems = new RadComboBoxItemCollection(new RadComboBox());

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (RadTextBox1.Enabled == false)
            {
                RadTextBox1.Enabled = false;
            }
            else
            {
                RadTextBox1.Enabled = true;
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            if (RadTextBox1.Enabled == false)
            {
                RadTextBox1.Enabled = false;
            }
            else
            {
                RadTextBox1.Enabled = true;
            }
        }
        protected void Page_InitComplete(object sender, EventArgs e)
        {
            if (RadTextBox1.Enabled == false)
            {
                RadTextBox1.Enabled = false;
            }
            else
            {
                RadTextBox1.Enabled = true;
            }
        }
        
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (RadTextBox1.Enabled == false)
            {
                RadTextBox1.Enabled = false;
            }
            else
            {
                RadTextBox1.Enabled = true;
            }
        }
        
        

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (RadTextBox1.Enabled == false)
            {
                RadTextBox1.Enabled = false;
            }
            else
            {
                RadTextBox1.Enabled = true;
            }
        }

        protected void Page_Prerender(object sender, EventArgs e)
        {
            if (RadTextBox1.Enabled == false)
            {
                RadTextBox1.Enabled = false;
            }
            else
            {
                RadTextBox1.Enabled = true;
            }
        }


        protected void Page_UnLoad(object sender, EventArgs e)
        {
            if (RadTextBox1.Enabled == false)
            {
                RadTextBox1.Enabled = false;
            }
            else
            {
                RadTextBox1.Enabled = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RadAjaxManager radAjaxManager = RadAjaxManager.GetCurrent(Page);

                if (radAjaxManager != null)
                {
                    RadTextBox radTextBox = (RadTextBox)FindControl("RadTextBox1");
                    RadComboBox radComboBox = (RadComboBox)FindControl("RadComboBox1");
                    Button button = (Button)FindControl("Button1");
                    Label label = (Label)FindControl("Label1");
                    radAjaxManager.AjaxSettings.AddAjaxSetting(button, label);
                    radAjaxManager.AjaxSettings.AddAjaxSetting(radComboBox, radTextBox);
                }


            }
           
            Button1.Click += Button1_Click;
            RadComboBox1.TextChanged += RadComboBox1_TextChanged;

           
        }

        protected void RadComboBox1_TextChanged(object sender, EventArgs e)
        {
            RadComboBox combo = (RadComboBox)sender;

            string selected = combo.SelectedValue;
            

            if (selected == null || selected == "")
            {
                if (RadTextBox1.Enabled)
                {
                    RadTextBox1.Enabled = false;
                }
            }
            else
            {
                RadTextBox1.Enabled = true;
                radComboBoxItems.Add(selected);
            }

            int count = radComboBoxItems.Count;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //Label1.Text = "Shhhiiittt";
        }

        protected void RadComboBox1_ItemsRequested(object sender, EventArgs e)
        {
            RadComboBox combo = (RadComboBox)sender;
            ReviewerRepository reviewerRepository = new ReviewerRepository(_connectionFactory);
            combo.DataSource = reviewerRepository.GetActiveReviewers();
            combo.DataTextField = "ReviewerName";
            combo.DataValueField = "ReviewerID";
            combo.DataBind();
        }

       
    }


    
}