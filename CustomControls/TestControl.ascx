﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestControl.ascx.cs" Inherits="GoodReadsTwo.CustomControls.TestControl" %>

<div>
    <telerik:RadTextBox ID="RadTextBox1" runat="server"></telerik:RadTextBox><br />
    <telerik:RadComboBox ID="RadComboBox1"
        runat="server"
        AllowCustomText="true"
        OnItemsRequested="RadComboBox1_ItemsRequested"
        EnableLoadOnDemand="true"
        AutoPostBack="true"
        Filter="Contains" OnTextChanged="RadComboBox1_TextChanged">
    </telerik:RadComboBox>
</div>

<div>
    <asp:Button ID="Button1" Text="Button" runat="server"></asp:Button><br />
    <asp:Label ID="Label1" runat="server"></asp:Label>
</div>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script>

    </script>
</telerik:RadScriptBlock>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script>
        
    </script>
</telerik:RadCodeBlock>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadComboBox1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadTextBox1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
