﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BookImage",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Name = c.String(),
                        Image = c.Binary(),
                        ContentType = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Book", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.Book",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Author = c.String(),
                        Genre_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Genre", t => t.Genre_ID)
                .Index(t => t.Genre_ID);
            
            CreateTable(
                "dbo.Genre",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BookImage", "ID", "dbo.Book");
            DropForeignKey("dbo.Book", "Genre_ID", "dbo.Genre");
            DropIndex("dbo.Book", new[] { "Genre_ID" });
            DropIndex("dbo.BookImage", new[] { "ID" });
            DropTable("dbo.Genre");
            DropTable("dbo.Book");
            DropTable("dbo.BookImage");
        }
    }
}
