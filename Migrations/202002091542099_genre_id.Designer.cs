﻿// <auto-generated />
namespace GoodReadsTwo.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class genre_id : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(genre_id));
        
        string IMigrationMetadata.Id
        {
            get { return "202002091542099_genre_id"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
