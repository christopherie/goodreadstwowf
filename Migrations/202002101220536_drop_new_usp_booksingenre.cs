﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class drop_new_usp_booksingenre : DbMigration
    {
        public override void Up()
        {
            Sql("DROP PROCEDURE dbo.BooksInGenre");
        }
        
        public override void Down()
        {
        }
    }
}
