﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class review_status : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Review", "Status", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Review", "Status");
        }
    }
}
