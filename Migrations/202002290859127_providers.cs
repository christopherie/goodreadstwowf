﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class providers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Provider",
                c => new
                    {
                        ProviderID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ProviderID);
            
            AddColumn("dbo.Employee", "ProviderID", c => c.Int(nullable: false));
            CreateIndex("dbo.Employee", "ProviderID");
            AddForeignKey("dbo.Employee", "ProviderID", "dbo.Provider", "ProviderID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employee", "ProviderID", "dbo.Provider");
            DropIndex("dbo.Employee", new[] { "ProviderID" });
            DropColumn("dbo.Employee", "ProviderID");
            DropTable("dbo.Provider");
        }
    }
}
