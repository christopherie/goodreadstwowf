﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class genre_id : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Book", "Genre_ID", "dbo.Genre");
            RenameColumn(table: "dbo.Book", name: "Genre_ID", newName: "Genre_GenreId");
            RenameIndex(table: "dbo.Book", name: "IX_Genre_ID", newName: "IX_Genre_GenreId");
            DropPrimaryKey("dbo.Genre");
            DropColumn("dbo.Genre", "ID");
            AddColumn("dbo.Genre", "GenreId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Genre", "GenreId");
            AddForeignKey("dbo.Book", "Genre_GenreId", "dbo.Genre", "GenreId");
        }
        
        public override void Down()
        {
           
        }
    }
}
