﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_review_reviewer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Review", "FirstReviewer", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Review", "FirstReviewer");
        }
    }
}
