﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class book_genre_full_references : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Book", "Genre_GenreId", "dbo.Genre");
            DropIndex("dbo.Book", new[] { "Genre_GenreId" });
            RenameColumn(table: "dbo.Book", name: "Genre_GenreId", newName: "GenreId");
            AlterColumn("dbo.Book", "GenreId", c => c.Int(nullable: false));
            CreateIndex("dbo.Book", "GenreId");
            AddForeignKey("dbo.Book", "GenreId", "dbo.Genre", "GenreId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Book", "GenreId", "dbo.Genre");
            DropIndex("dbo.Book", new[] { "GenreId" });
            AlterColumn("dbo.Book", "GenreId", c => c.Int());
            RenameColumn(table: "dbo.Book", name: "GenreId", newName: "Genre_GenreId");
            CreateIndex("dbo.Book", "Genre_GenreId");
            AddForeignKey("dbo.Book", "Genre_GenreId", "dbo.Genre", "GenreId");
        }
    }
}
