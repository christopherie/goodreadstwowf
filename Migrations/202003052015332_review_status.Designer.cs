﻿// <auto-generated />
namespace GoodReadsTwo.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class review_status : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(review_status));
        
        string IMigrationMetadata.Id
        {
            get { return "202003052015332_review_status"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
