﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class book_review_relationship : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Review",
                c => new
                    {
                        ReviewID = c.Int(nullable: false, identity: true),
                        Rating = c.Int(nullable: false),
                        Content = c.String(),
                        ReviewDate = c.DateTime(nullable: false),
                        Book_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ReviewID)
                .ForeignKey("dbo.Book", t => t.Book_ID)
                .Index(t => t.Book_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Review", "Book_ID", "dbo.Book");
            DropIndex("dbo.Review", new[] { "Book_ID" });
            DropTable("dbo.Review");
        }
    }
}
