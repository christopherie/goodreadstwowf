﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remove_first_reviewer : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Review", "FirstReviewer");
        }
        
        public override void Down()
        {
        }
    }
}
