﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class book_review_rating_datatype : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Review", "Rating", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Review", "Rating", c => c.Int(nullable: false));
        }
    }
}
