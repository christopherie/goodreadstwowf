﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class member_review_reviewer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Member",
                c => new
                    {
                        MemberID = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.MemberID);
            
            CreateTable(
                "dbo.Reviewer",
                c => new
                    {
                        ReviewerID = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.ReviewerID);
            
            AddColumn("dbo.Review", "Member_MemberID", c => c.String(maxLength: 128));
            AddColumn("dbo.Review", "Reviewer_ReviewerID", c => c.String(maxLength: 128));
            CreateIndex("dbo.Review", "Member_MemberID");
            CreateIndex("dbo.Review", "Reviewer_ReviewerID");
            AddForeignKey("dbo.Review", "Member_MemberID", "dbo.Member", "MemberID");
            AddForeignKey("dbo.Review", "Reviewer_ReviewerID", "dbo.Reviewer", "ReviewerID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Review", "Reviewer_ReviewerID", "dbo.Reviewer");
            DropForeignKey("dbo.Review", "Member_MemberID", "dbo.Member");
            DropIndex("dbo.Review", new[] { "Reviewer_ReviewerID" });
            DropIndex("dbo.Review", new[] { "Member_MemberID" });
            DropColumn("dbo.Review", "Reviewer_ReviewerID");
            DropColumn("dbo.Review", "Member_MemberID");
            DropTable("dbo.Reviewer");
            DropTable("dbo.Member");
        }
    }
}
