﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class brand_new_usp_booksingenre : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                       CREATE PROCEDURE BooksInGenre(@genreId INT)
                       AS
                       BEGIN

                         SET NOCOUNT ON

	                     SELECT B.ID, B.Title, B.Author, BI.Name, BI.ContentType, BI.Image
	                     FROM [GoodReadsTwo].[dbo].[Book] AS B
	                     JOIN [GoodReadsTwo].[dbo].[BookImage] AS BI
	                     ON B.ID = BI.ID
	                     WHERE B.GenreId = @genreId

                      END
                     ");
        }
        
        public override void Down()
        {
            Sql("DROP PROCEDURE BooksInGenre");
        }
    }
}
