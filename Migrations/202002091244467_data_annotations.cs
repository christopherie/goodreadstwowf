﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class data_annotations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Book", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Book", "Author", c => c.String(nullable: false));
            AlterColumn("dbo.Genre", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Genre", "Name", c => c.String());
            AlterColumn("dbo.Book", "Author", c => c.String());
            AlterColumn("dbo.Book", "Title", c => c.String());
        }
    }
}
