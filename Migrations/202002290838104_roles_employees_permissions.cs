﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class roles_employees_permissions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employee",
                c => new
                    {
                        EmployeeID = c.String(nullable: false, maxLength: 128),
                        RoleID = c.Int(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.EmployeeID)
                .ForeignKey("dbo.Role", t => t.RoleID, cascadeDelete: true)
                .Index(t => t.RoleID);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        RoleID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.RoleID);
            
            CreateTable(
                "dbo.Permission",
                c => new
                    {
                        PermissionID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.PermissionID);
            
            CreateTable(
                "dbo.PermissionRole",
                c => new
                    {
                        Permission_PermissionID = c.Int(nullable: false),
                        Role_RoleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Permission_PermissionID, t.Role_RoleID })
                .ForeignKey("dbo.Permission", t => t.Permission_PermissionID, cascadeDelete: true)
                .ForeignKey("dbo.Role", t => t.Role_RoleID, cascadeDelete: true)
                .Index(t => t.Permission_PermissionID)
                .Index(t => t.Role_RoleID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PermissionRole", "Role_RoleID", "dbo.Role");
            DropForeignKey("dbo.PermissionRole", "Permission_PermissionID", "dbo.Permission");
            DropForeignKey("dbo.Employee", "RoleID", "dbo.Role");
            DropIndex("dbo.PermissionRole", new[] { "Role_RoleID" });
            DropIndex("dbo.PermissionRole", new[] { "Permission_PermissionID" });
            DropIndex("dbo.Employee", new[] { "RoleID" });
            DropTable("dbo.PermissionRole");
            DropTable("dbo.Permission");
            DropTable("dbo.Role");
            DropTable("dbo.Employee");
        }
    }
}
