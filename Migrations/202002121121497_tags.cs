﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tags : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tag",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.TagBook",
                c => new
                    {
                        Tag_TagID = c.Int(nullable: false),
                        Book_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_TagID, t.Book_ID })
                .ForeignKey("dbo.Tag", t => t.Tag_TagID, cascadeDelete: true)
                .ForeignKey("dbo.Book", t => t.Book_ID, cascadeDelete: true)
                .Index(t => t.Tag_TagID)
                .Index(t => t.Book_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TagBook", "Book_ID", "dbo.Book");
            DropForeignKey("dbo.TagBook", "Tag_TagID", "dbo.Tag");
            DropIndex("dbo.TagBook", new[] { "Book_ID" });
            DropIndex("dbo.TagBook", new[] { "Tag_TagID" });
            DropTable("dbo.TagBook");
            DropTable("dbo.Tag");
        }
    }
}
