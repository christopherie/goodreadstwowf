﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class reviewer_key : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Review", name: "Reviewer_ReviewerID", newName: "ReviewerID");
            RenameIndex(table: "dbo.Review", name: "IX_Reviewer_ReviewerID", newName: "IX_ReviewerID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Review", name: "IX_ReviewerID", newName: "IX_Reviewer_ReviewerID");
            RenameColumn(table: "dbo.Review", name: "ReviewerID", newName: "Reviewer_ReviewerID");
        }
    }
}
