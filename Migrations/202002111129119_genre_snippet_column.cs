﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class genre_snippet_column : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Genre", "Snippet", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Genre", "Snippet");
        }
    }
}
