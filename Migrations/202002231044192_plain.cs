﻿namespace GoodReadsTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class plain : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Review", "FirstReviewer");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Review", "FirstReviewer", c => c.String());
        }
    }
}
