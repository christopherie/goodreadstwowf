﻿using GoodReadsTwo.Repository;
using HouseofCat.DependencyInjection.WebForms.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using Unity;

namespace GoodReadsTwo
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var container = this.AddUnity();
            container.RegisterType<IUnitOfWork, UnitOfWork>();
            container.RegisterType<IGenreRepository, GenreRepository>();
            container.RegisterType<IBookRepository, BookRepository>();
            container.RegisterType<IBookImageRepository, BookImageRepository>();
            container.RegisterType<ITagRepository, TagRepository>();
            container.RegisterType<IRoleRepository, RoleRepository>();
            container.RegisterType<IPermissionRepository, PermissionRepository>();
            container.RegisterType<IEmployeeRepository, EmployeeRepository>();
            container.RegisterType<IReviewRepository, ReviewRepository>();

        }
    }
}