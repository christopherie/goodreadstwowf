﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using GoodReadsTwo.Repository;
using System;
using System.Diagnostics;
using System.Linq;
using System.Web.UI;

namespace GoodReadsTwo
{
    public partial class Search : System.Web.UI.Page
    {
        private readonly GoodReadsContext _goodReadsContext;
        private readonly UnitOfWork _unitOfWork;

        public Search()
        {
            _goodReadsContext = new GoodReadsContext();
            _unitOfWork = new UnitOfWork(_goodReadsContext);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int queryId = Convert.ToInt32(Page.Request.QueryString.Get("id"));

            _goodReadsContext.Database.Log = l => Debug.Write(l);

            Book book = _unitOfWork.Repository<Book>().Get(b => b.ID == queryId, b => b.BookImage, b => b.Reviews, b => b.Tags);

            Genre genre = _unitOfWork.Repository<Genre>().Find(g => g.GenreId == book.GenreId).Single();

            RadLabel1.Text = genre.Snippet;

            RadLabel2.Text = book.Title;

            RadLabel3.Text = book.Author;

            RadBinaryImage1.DataValue = book.BookImage.Image;

            Repeater1.DataSource = book.Reviews;

            Repeater1.DataBind();


        }

        protected void RadButton1_Click(object sender, EventArgs e)
        {
            int queryId = Convert.ToInt32(Page.Request.QueryString.Get("id"));

            Response.Redirect("BookEdit?editId=" + queryId, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        public double GetRating(object rating)
        {
            return Convert.ToDouble(rating);
        }

        public string FormatDate(object reviewDate)
        {
            DateTime dateTime = (DateTime)reviewDate;
            return dateTime.ToString("yyyy-MM-dd");
        }
    }
}