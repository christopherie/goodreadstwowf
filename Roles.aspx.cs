﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodReadsTwo
{
    public partial class Roles : System.Web.UI.Page
    {
        RoleRepository _roleRepository;

        public Roles()
        {
            ConnectionFactory connectionFactory = new ConnectionFactory();
            _roleRepository = new RoleRepository(connectionFactory);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RadLabel1.Text = "Role";
            }

            RadButton1.Click += RadButton1_Click;
        }

        private void RadButton1_Click(object sender, EventArgs e)
        {
            string role = RadTextBox1.Text.Trim();
            _roleRepository.Insert(role);
        }
    }
}