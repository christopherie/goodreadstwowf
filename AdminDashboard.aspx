﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminDashboard.aspx.cs" Inherits="GoodReadsTwo.AdminDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script src="Scripts/Custom/admindashboard.js">
        
    </script>
    

        <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="True" AllowSorting="True" AllowMultiRowSelection="true">
            <GroupingSettings CollapseAllTooltip="Collapse all groups"></GroupingSettings>

            <ClientSettings Selecting-AllowRowSelect="true">
                <Selecting AllowRowSelect="True"></Selecting>
            </ClientSettings>
            <MasterTableView AutoGenerateColumns="true" DataKeyNames="ReviewID">

                <Columns>

                    <telerik:GridTemplateColumn HeaderText="Assign" UniqueName="AssignColumn">
                        <ItemTemplate>
                            <telerik:RadCheckBox ID="CheckBox1" AutoPostBack="true"
                                OnCheckedChanged="CheckBox1_CheckChanged" runat="server" Enabled="false">
                            </telerik:RadCheckBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Select Reviewer">
                        <ItemTemplate>
                            <telerik:RadComboBox ID="RadComboBox1" runat="server" AllowCustomText="True"
                                OnItemsRequested="RadComboBox1_ItemsRequested" EnableLoadOnDemand="true"
                                Filter="Contains" OnClientTextChange="RadComboBox1_ClientTextChanged">
                            </telerik:RadComboBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                </Columns>
            </MasterTableView>
        </telerik:RadGrid>



</asp:Content>
