﻿using GoodReadsTwo.DAL;
using GoodReadsTwo.Model;
using GoodReadsTwo.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodReadsTwo
{
    public partial class AllBooks : System.Web.UI.Page
    {
        public Repeater Repeater { get; }
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        [WebMethod]
        public static IEnumerable<Book> GetBookData()
        {
            GoodReadsContext goodReadsContext = new GoodReadsContext();
            UnitOfWork unitOfWork = new UnitOfWork(goodReadsContext);
            IEnumerable<Book> allBooks = unitOfWork.Repository<Book>().GetAll();
            return allBooks;
        }
    }
}